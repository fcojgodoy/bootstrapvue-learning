import Vue from 'vue'
import App from './App.vue'
import router from './router'

// Importing BootstrapVue
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Importing FontAwesomeIcon
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import FontAwasomeIcon from './fontAwesomeIcon'
FontAwasomeIcon.init()

Vue.config.productionTip = false

// Use BootstrapVue
Vue.use(BootstrapVue)

// Component FontAwasomeIcon
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
