// Importing library
import { library } from '@fortawesome/fontawesome-svg-core'

// Importing icons from free solid style
import {
  faUserSecret, faQuestion
} from '@fortawesome/free-solid-svg-icons'

// Adding icons
export default {
  init () {
    library.add(
      faUserSecret,
      faQuestion
    )
  }
}
